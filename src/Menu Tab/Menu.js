import React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
// bikin tombol search
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
// import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';

// bikin table
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import { Container } from '@mui/system';

// icon ditable
import PostAddIcon from '@mui/icons-material/PostAdd';




// import { lightBlue, pink } from '@mui/material/colors';
 
const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }, 
});

// const serviceList = [
//     'Rute Operasional',
//     'Briefing',
//     'Go/No Go Item',
//     'Riwayat Catatan',
//     'Serah Terima Dinasan',
//     'Bantuan Darurat',
// ];

// {serviceList.map((service) => (
//     <Typography color='#ECAB55' position='flex' elevation={3}>{service}</Typography>
// ))}

function Menu() {
    return (
        <ThemeProvider theme={theme}>
            <Box bgcolor='#303F51'>
                    <Container maxWidth="xl">
                        <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Rute Operasional</Typography>
                    </Container>
                    <Box>
                        <Grid container spacing={1} justifyContent='center'>
                            <Button>   
                            <Grid item>
                                <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                    <Typography variant='subtitle2' color='#ECAB55'>Rute Operasional</Typography>
                                   
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Briefing</Typography>
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Go/No Go Item</Typography>
                            </Grid> 
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>  
                            <Grid item>
                                <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Riwayat Catatan</Typography>
                            </Grid>   
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                               
                            </Grid>  
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <WarningAmberRoundedIcon color='primary'sx={{width: 60 , height: 60 , margin: 3.5}} />
                                <Typography variant='subtitle2'>Bantuan Darurat</Typography>
                            </Grid>
                            </Button>
                        </Grid>
                    </Box>
                       
                        <Grid container justifyContent='center'>
                            {/* tombol search */}
                            <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, marginTop: 2 }}>
                                {/* <IconButton sx={{ p: '10px' }} aria-label="menu">
                                    <MenuIcon />
                                </IconButton> */}
                                <InputBase
                                    sx={{ ml: 1, flex: 1 }}
                                    placeholder="Pencarian"
                                    inputProps={{ 'aria-label': 'Pencarian' }}
                                />
                                    <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                        <SearchIcon/>
                                    </IconButton>

                            </Paper>
                        </Grid>

                    <Box>
                        <Typography 
                        sx={{fontFamily: 'Montserrat', marginTop: 4}} 
                        color='white'
                        align='center'
                        >RUTE OPERASIONAL</Typography> 
                    </Box>   

                    <Container maxWidth="xl">
                    <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
                            <Typography sx={{fontWeight: 400}} color='white'>Senin, 02 Jan 2023</Typography> 
                            <Typography sx={{fontWeight: 400}} color='white'>Kalideres (KDL 023) - Ponogoro (PNG 013)</Typography> 
                        <Box sx={{display: 'flex'}}> 
                            <Typography sx={{fontWeight: 400}} color='white'>No.Bus</Typography> 
                            <Typography sx={{ml: 1, fontFamily: 'Montserrat'}} color='white'>BM 007</Typography> 
                        </Box>
                    </Box>
                    <Divider sx={{borderColor: 'white', pt: 2}}/> 

                    {/* mumbuat table */}
                    <TableContainer component={Paper}>
                        <Table arial-label='simple table'>
                            <TableHead sx={{backgroundColor: '#303F51'}}>
                                <TableRow>
                                    <TableCell sx={{color: 'white'}} align='center'>KODE</TableCell>
                                    <TableCell sx={{color: 'white'}}>LOKASI</TableCell>
                                    <TableCell sx={{color: 'white'}} align='center'>JARAK (km)</TableCell>
                                    <TableCell sx={{color: 'white'}} align='center'>WAKTU TEMPUH</TableCell>
                                    <TableCell sx={{color: 'white'}} align='center'>WAKTU REALISASI</TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {tableData.map(row => (
                                    <TableRow 
                                        key={row.kode}
                                        sx={{'&:last-child td, &:last-child th' : { border: 0} }}
                                    >
                                        <TableCell align='center'>{row.kode}</TableCell>
                                        <TableCell>{row.lokasi}</TableCell>
                                        <TableCell align='center'>{row.jarak}</TableCell>
                                        <TableCell align='center'>{row.waktu_tempuh}</TableCell>
                                        <TableCell align='center'>{row.waktu_relasi}</TableCell>
                                        <TableCell align='center' bgcolor='red' sx={{width: 40}} >
                                        
                                                <Button
                                                    // variant="contained"
                                                    size='large'
                                                    
                                                    sx={{
                                                        mr: 1, 
                                                        width: 110,
                                                        height: '100%',
                                                        // backgroundColor: "red",
                                                        "&:hover": { backgroundColor: "red"},
                                                        fontFamily: 'roboto',
                                                        }}
                                                    >
                                                <Typography sx={{ fontFamily: 'roboto', fontSize: 12 }}>
                                                 Mulai 
                                                 Perjalanan
                                                 </Typography>
                                                </Button> 
                                        </TableCell>


                                        <TableCell bgcolor="green" sx={{width: 40}}>
                 
                                                <Button
                                                    // variant="contained"
                                                    size='large'
                                                    // color="success"
                                                    sx={{
                                                        mr: 1, 
                                                        width: 110,
                                                        height: '100%',
                                                        "&:hover": { backgroundColor: "warning.dark"},
                                                        fontFamily: 'roboto',
                                                    }}
                                                    >
                                                <PostAddIcon  fontSize="large"/>
                                                <Typography sx={{ ml: 1 , fontFamily: 'roboto', fontSize: 10 }}>      
                                                Tambah
                                                Catatan
                                                </Typography>
                                                </Button> 
                                        </TableCell>
                                            
                                    
                                    </TableRow>
                              
                                ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Box pt={0.3}>
                        <Button 
                            variant="contained"
                            color="success"
                            sx={{width: 1301, height: 50 }}
                            >
                            <PostAddIcon/>
                            <Typography sx={{ml: 0.3, fontFamily: 'Montserrat'}} >Preliminary Report</Typography>
                        </Button>
                    </Box>
                    </Container>  
                    
                    <Container sx={{ml: 2}}>
                        <Box pt={1.5}>
                        <Button 
                            variant="contained"
                            color="success"
                            sx={{width: 1270, height: 80}}               
                            >
                            <Typography sx={{fontFamily: 'Montserrat'}} >Selesai Operasional</Typography>
                        </Button>
                        </Box>
                        <Box>
                            <Typography color='transparent'>row</Typography>
                        </Box>
                    </Container>        
            </Box>
        </ThemeProvider>
    );
}

export default Menu;

const tableData = [{
    "kode": "KLD023",
    "lokasi": "TERM.KALIDERES",
    "jarak": "-",
    "waktu_tempuh": "-",
    "waktu_relasi": "-",
  }, {
    "kode": "BTM001",
    "lokasi": "AGEN BEKASI TIMUR",
    "jarak": "57.3",
    "waktu_tempuh": "01:05:00",
    "waktu_relasi": "01:25:00",
  }, {
    "kode": "KRW017",
    "lokasi": "AGEN KARAWANG",
    "jarak": "46.3",
    "waktu_tempuh": "00:58:00",
    "waktu_relasi": "00:55:00",
  }, {
    "kode": "GRS005",
    "lokasi": "REST AREA GRINGSING",
    "jarak": "340",
    "waktu_tempuh": "04:14:00",
    "waktu_relasi": "04:40:00",
  }, {
    "kode": "SOL020",
    "lokasi": "SOLO",
    "jarak": "147",
    "waktu_tempuh": "02:08:00",
    "waktu_relasi": "02:05:00",
  }, {
    "kode": "WNG024",
    "lokasi": "WONOGIRI",
    "jarak": "53.2",
    "waktu_tempuh": "01:34:00",
    "waktu_relasi": "01:30:00",
  }, {
    "kode": "PNG013",
    "lokasi": "PONOROGO",
    "jarak": "75.3",
    "waktu_tempuh": "01:57:00",
    "waktu_relasi": "02:00:00",
  }]